import java.util.Scanner;

public class Uebungsblatt_AB_KonsoleneingabeAufgabe2 {
	 public static void main(String[] args) {
	
	 // Neues Scanner-Objekt myScanner wird erstellt
	Scanner namenscanner = new Scanner (System.in);
	
	System.out.println("Guten Tag, wie ist Ihr Name ");
	
	String s = namenscanner.nextLine();
	
	System.out.println("Bitte geben Sie Ihr Alter ein ");
	
	int alter = namenscanner.nextInt();
	
	System.out.println("Ihr Name ist "+ s);
	System.out.println("Ihr alter ist " + alter);
	
	namenscanner.close();
	
	}
}
