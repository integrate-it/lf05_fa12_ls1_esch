import java.util.Scanner; // Import der Klasse Scanner
public class Uebungsblatt_AB_Konsoleneingabe
{

 public static void main(String[] args) // Hier startet das Programm
 {

 // Neues Scanner-Objekt myScanner wird erstellt
 Scanner myScanner = new Scanner(System.in);

 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

 // Die Variable zahl1 speichert die erste Eingabe
 int zahl1 = myScanner.nextInt();

 System.out.println("Bitte geben Sie eine zweite ganze Zahl ein: ");

 // Die Variable zahl2 speichert die zweite Eingabe
 int zahl2 = myScanner.nextInt();

 // Die Addition der Variablen zahl1 und zahl2
 // wird der Variable ergebnis zugewiesen.
 int ergebnis = zahl1 + zahl2;

 System.out.print("\n\n\nErgebnis der Addition lautet: ");
 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);


 
 //hier startet minus
 


 System.out.println("Bitte geben Sie eine ganze Zahl ein: ");

 // Die Variable zahl3 speichert die erste Eingabe
 int zahl3 = myScanner.nextInt();

 System.out.println("Bitte geben Sie eine zweite ganze Zahl ein: ");

 // Die Variable zahl4 speichert die zweite Eingabe
 int zahl4 = myScanner.nextInt();

 // Die subtraktion der Variablen zahl1 und zahl2
 // wird der Variable ergebnis zugewiesen.
 int ergebnis2 = zahl3 - zahl4;

 System.out.println("\n\n\nErgebnis der subtraktion lautet: ");
 System.out.println(zahl3 + " - " + zahl4 + " = " + ergebnis2);
 
 //hier multiplizieren
 
 System.out.println("Bitte geben Sie eine ganze Zahl ein: ");

 // Die Variable zahl3 speichert die erste Eingabe
 int zahl5 = myScanner.nextInt();

 System.out.println("Bitte geben Sie eine zweite ganze Zahl ein: ");

 // Die Variable zahl4 speichert die zweite Eingabe
 int zahl6 = myScanner.nextInt();

 // Die multiplikation der Variablen zahl1 und zahl2
 // wird der Variable ergebnis zugewiesen.
 int ergebnis3 = zahl5 * zahl6;

 System.out.println("\n\n\nErgebnis der multiplikation lautet: ");
 System.out.println(zahl3 + " * " + zahl4 + " = " + ergebnis3);
 
 //ab Hier Teilen
 
 System.out.println("Bitte geben Sie eine ganze Zahl ein: ");

 // Die Variable zahl7 speichert die erste Eingabe
 int zahl7 = myScanner.nextInt();

 System.out.println("Bitte geben Sie eine zweite ganze Zahl ein: ");

 // Die Variable zahl8 speichert die zweite Eingabe
 int zahl8 = myScanner.nextInt();

 // Die division der Variablen zahl7 und zahl8
 // wird der Variable ergebnis zugewiesen.
 int ergebnis4 = zahl7 / zahl8;

 System.out.println("\n\n\nErgebnis der divison lautet: ");
 System.out.println(zahl7 + " / " + zahl8 + " = " + ergebnis4);





 myScanner.close();


 }
}